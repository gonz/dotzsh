export PATH="$HOME/.local/bin:$HOME/.local/share/hifi/scripts:$HOME/.cache/rebar3/bin/:$HOME/.gem/ruby/2.4.0/bin:$HOME/.exenv/bin:$HOME/.config/wifixir/script:$HOME/.config/pyfi/script:$HOME/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl"
# vim as standard editor
export EDITOR="vim"
export XFLUXSCRIPTS="/home/gonz/code/bash/xflux-scripts"
# old stuff that's needed for crusty ERL libs
export ERL_LIBS="/home/gonz/tools/proper"
# GB date and time
export LC_TIME="en_GB.utf8"
